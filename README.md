# README #

This README would normally document whatever steps are necessary to get your application up and running.

#Steps to setup the repo on your local
1. Clone the repository using terminal command. Note: Edit your "username" inside the link
"git clone https://username@bitbucket.org/virenderjangra/digit-jigsaw.git"
2. Enter to the digit-jigsaw directory using
"cd digit-jigsaw"
3. Edit/Create new files to the folder.
4. Then add your new/all files to commit
"git add \*"
5. Pull the differences from the git first
"git pull"
6. Commit the changes to the repository
"git commit -m 'YOUR MESSAGE DURING COMMIT'"
7. Then finally push the files to the master branch
"git push"


# Server Folder Location:
https://cust-sol-in.blippar.com/digit-jigsaw/
